name: freedesktop-sdk

format-version: 12

aliases:
  freedesktop: https://gitlab.freedesktop.org/
  freedesktop-old: https://anongit.freedesktop.org/git/
  ftp_gnu_org: https://ftp.gnu.org/gnu/
  gnome: https://gitlab.gnome.org/GNOME/

mirrors:
  - name: kernel_org
    aliases:
      ftp_gnu_org:
      - https://mirrors.kernel.org/gnu/

element-path: elements

fail-on-overlap: true

variables:
  builddir: bst_build_dir
  conf-deterministic: |
    --enable-deterministic-archives
  conf-link-args: |
    --enable-shared \
    --disable-static
  conf-host: |
    --host=%{host-triplet}
  conf-build: |
    --build=%{build-triplet}
  host-triplet: "%{triplet}"
  build-triplet: "%{triplet}"
  sbindir: "%{bindir}"
  sysconfdir: "%{prefix}/etc"
  localstatedir: "%{prefix}/var"
  branch: "18.08"
  lib: "lib/%{gcc_triplet}"
  indep-libdir: "%{prefix}/lib"
  debugdir: "%{indep-libdir}/debug"
  sourcedir: "%{debugdir}/source"
  gcc_triplet: "%{gcc_arch}-linux-%{abi}"
  triplet: "%{arch}-unknown-linux-%{abi}"
  gcc_arch: "%{arch}"
  abi: "gnu"
  (?):
    - target_arch == "i586":
        gcc_arch: "i386"
    - target_arch == "arm":
        abi: "gnueabihf"
  ca_path: "%{sysconfdir}/ssl/certs/ca-certificates.crt"

  strip-binaries: |
    touch source-files
    find "%{install-root}" -type f \
      '(' -perm -111 -o -name '*.so*' \
          -o -name '*.cmxs' -o -name '*.node' ')' \
          -print0 | while read -r -d $'\0' file; do
      read -n4 hdr <"${file}" || continue # check for elf header
      if [ "$hdr" != "$(printf \\x7fELF)" ]; then
        continue
      fi
      if objdump -j .gnu_debuglink -s "${file}" &>/dev/null; then
        continue
      fi
      case "${file}" in
        "%{install-root}%{debugdir}/"*)
          continue
          ;;
        *)
          ;;
      esac
      realpath="$(realpath -s --relative-to="%{install-root}" "${file}")"
      debugfile="%{install-root}%{debugdir}/${realpath}.debug"
      mkdir -p "$(dirname "$debugfile")"
      debugedit -i --list-file=source-files.part --base-dir="%{build-root}" --dest-dir="%{debugdir}/source/%{element-name}" "${file}"
      cat source-files.part >>source-files
      objcopy %{objcopy-extract-args} "${file}" "$debugfile"
      chmod 644 "$debugfile"
      mode="$(stat -c 0%a "${file}")"
      [ -w "${file}" ] || chmod +w "${file}"
      strip %{strip-args} "${file}"
      objcopy %{objcopy-link-args} "$debugfile" "${file}"
      chmod "${mode}" "${file}"
    done
    sort -zu  <source-files | while read -r -d $'\0' source; do
      dst="%{install-root}%{debugdir}/source/%{element-name}/${source}"
      src="%{build-root}/${source}"
      if [ -d "${src}" ]; then
        install -m0755 -d "${dst}"
        continue
      fi
      [ -f "${src}" ] || continue
      install -m0644 -D "${src}" "${dst}"
    done

environment:
  CPPFLAGS: "-O2 -D_FORTIFY_SOURCE=2"
  CFLAGS: "-O2 -g -fstack-protector-strong"
  CXXFLAGS: "-O2 -g -fstack-protector-strong"
  LDFLAGS: "-fstack-protector-strong -Wl,-z,relro,-z,now"
  LC_ALL: en_US.UTF-8
  PYTHON: "%{bindir}/python3"

split-rules:
  devel:
    - "%{includedir}"
    - "%{includedir}/**"
    - "%{libdir}/pkgconfig"
    - "%{libdir}/pkgconfig/**"
    - "%{datadir}/pkgconfig"
    - "%{datadir}/pkgconfig/**"
    - "%{datadir}/aclocal"
    - "%{datadir}/aclocal/**"
    - "%{prefix}/lib/cmake"
    - "%{prefix}/lib/cmake/**"
    - "%{libdir}/cmake"
    - "%{libdir}/cmake/**"
    - "%{prefix}/lib/*.a"
    - "%{libdir}/*.a"

  debug:
    - "%{debugdir}/**"

plugins:
  - origin: local
    path: plugins/sources
    sources:
      crate: 0

  - origin: pip
    package-name: buildstream-external
    elements:
      collect-integration: 0
      flatpak_image: 0
      x86image: 0

options:
  bootstrap_build_arch:
    type: arch
    description: Architecture
    variable: bootstrap_build_arch
    values:
      - arm
      - aarch64
      - i586
      - x86_64

  target_arch:
    type: arch
    description: Architecture
    variable: arch
    values:
      - arm
      - aarch64
      - i586
      - x86_64

artifacts:
  url: https://testcache.codethink.co.uk:11001

elements:
  cmake:
    variables:
      generator: Ninja
  autotools:
    variables:
      remove_libtool_modules: "true"
      remove_libtool_libraries: "true"
      delete_libtool_files: |
          find "%{install-root}" -name "*.la" -print0 | while read -d '' -r file; do
            if grep '^shouldnotlink=yes$' "${file}" &>/dev/null; then
              if %{remove_libtool_modules}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            else
              if %{remove_libtool_libraries}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            fi
          done
      conf-global: |
        %{conf-deterministic} \
        %{conf-link-args} \
        %{conf-build} \
        %{conf-host}
      conf-cmd: configure
    config:
      configure-commands:
        - |
          %{autogen}
          if [ -n "%{builddir}" ]; then
            mkdir %{builddir}
            cd %{builddir}
              reldir=..
            else
              reldir=.
          fi
          ${reldir}/%{configure}

      build-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make}

      install-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make-install}

        - |
          %{delete_libtool_files}
